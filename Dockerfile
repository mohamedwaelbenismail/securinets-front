FROM nginx:latest
COPY dist/securinets-front/ /usr/share/nginx/html
COPY nginx.conf /etc/nginx/conf.d/default.conf
