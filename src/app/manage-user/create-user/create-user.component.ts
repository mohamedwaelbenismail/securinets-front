import { Component, OnInit } from '@angular/core';
import { NgModel } from '@angular/forms';
import { User } from 'src/app/shared/models/User';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import {UserService} from '../../shared/services/user.service';


declare var swal ;
@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit {
  user: User = new User();
  busy: Subscription;
  isSubmitted = false ;
  passwordError = false ;
  constructor(private userService: UserService,
    private router: Router) { }

  ngOnInit(): void {
  }


  checkEmptyFields() {
    return (
      !this.user.first_name ||
      !this.user.last_name ||
      !this.user.email ||
      !this.user.password || 
      !this.user.membership_index
    );
  }
  checkPassword() {
    let numdigits = 0;
    let numUperCase = 0;
    let numLowerCase = 0;
    let i = 0 ;
    if (this.user.password.length < 8) {
      return false;
    }
    while ( (numdigits < 1  || numUperCase < 1 || numLowerCase < 1)  && (i <= this.user.password.length - 1)) {
          const char = this.user.password.charAt(i) ;
          if (!isNaN( parseInt(char, 10) )){
            numdigits++ ;

          } else if (char === char.toUpperCase()) {
            numUperCase++ ;
          } else if (char === char.toLowerCase()) {
             numLowerCase++  ;
          }
          i++;
      }
    if (numdigits < 1 || numUperCase < 1 || numLowerCase < 1) {
        return false;
      }
    return true ;
  }

  submit() {
    this.user.privilege_id = 3 ;
    this.isSubmitted = true ;
    if (this.checkEmptyFields()) {
      swal('error', 'Error while registering', 'error');
      if (!this.checkPassword()){
        this.passwordError = true;
      }
      return ;
    }
    else if (!this.checkPassword()) {
      swal('error', 'Problem with password pattern', 'error');
      this.passwordError = true;
      return ;
    }
    this.passwordError = false;

    this.busy = this.userService.addUser(this.user).subscribe((response) => {
      swal('success', 'You have been registred successfully. \n Please check your email and Verify your account', 'success');
      this.router.navigate(['auth/login']);
    }, (error) => {
      swal('error', error.error.response, 'error');
    } );
  }
}
