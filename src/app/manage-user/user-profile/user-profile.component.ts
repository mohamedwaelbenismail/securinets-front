import { Component, OnInit } from '@angular/core';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { User } from 'src/app/shared/models/User';
import { Subscription } from 'rxjs';
import { UserService } from 'src/app/shared/services/user.service';
import { StorageService } from 'src/app/shared/services/storage.service';
import { ActivatedRoute } from '@angular/router';
declare var swal ;
@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {
  user = new User();
  busy: Subscription;
  email: string;

  url: string | ArrayBuffer = '';
  editor: any;
  showModal: boolean;
  imageChangedEvent: any = '';
  croppedImage: any = '';
  user_id: number;
  constructor(private userService: UserService,
              private activatedRoute: ActivatedRoute,
              private storageService: StorageService
              ) {
               this.user_id = parseInt(this.activatedRoute.snapshot.queryParams.id,10);
  }

  ngOnInit() {
         // check if the user_id in the params and the user_id connected is the same
    

  }
  show() {
    this.showModal = true; // Show-Hide Modal Check
  }
  hide() {
    this.showModal = false;
  }
  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
  }
  imageLoaded() {
    // show cropper
  }
  cropperReady() {
    // cropper ready
  }
  loadImageFailed() {
    // show message
  }
  uploadUserPhoto(event) {
    const file = event.target.files[0];
    console.log(file);
     let formData = new FormData();
    formData.append('file', file);
    // this.userService.uploadPhotoProfile(formData).subscribe(
    //   data => console.log(data)
    // );
  }
  update() {
    // if (this.email != null) {
    //   this.userService.modifUserInfo(this.user).subscribe(
    //     data => {
    //       swal('Success!', 'Infos Updated', 'success');
    //     },
    //     error => {
    //        swal('Error!', 'The request failed!', 'error');
    //     }
    //   );
    // } else {
    //   swal('Error!', 'Email Required!', 'error');
    // }
  }
  
}
