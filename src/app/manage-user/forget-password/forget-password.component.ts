import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/shared/services/user.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
declare var swal ;
@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.scss']
})

export class ForgetPasswordComponent implements OnInit {
  emailRequest = '';
  isSubmited = false;
  busy: Subscription;
  constructor(private userService: UserService,private router: Router) { }

  ngOnInit(): void {
  }
  SubmitEmail() {
    this.isSubmited = true ;
    if (this.emailRequest.length === 0 ) {
      swal('error' , 'fields required', 'error');
      return ;
    }
    const that = this ;
    this.busy = this.userService.forgetPassword(this.emailRequest).subscribe((
      (data) => {
        swal('success' , 'You have recieved your password by email', 'success');
      }

    ), (error) => {
      swal('error' , 'error', 'error');
    });
  }

}
