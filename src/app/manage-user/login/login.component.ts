import { Component, OnInit } from '@angular/core';
import { switchAll } from 'rxjs/operators';
import { AuthService } from 'src/app/shared/services/auth.service';
import { UserService } from 'src/app/shared/services/user.service';
import { StorageService } from 'src/app/shared/services/storage.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Credentials } from 'src/app/shared/models/credentials';
import { Subscription } from 'rxjs';
import { User } from 'src/app/shared/models/User';
import {NgxSpinnerService} from 'ngx-spinner';



declare var swal ;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  isSubmitted = false;
  credentials: Credentials = new Credentials();
  busy: Subscription;
  user: User ;
  constructor(private authService: AuthService,
              private userService: UserService,
              private storageService: StorageService,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private spinner: NgxSpinnerService) {
}


  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe((params) => {
      if (params['confirm']) {
        swal('success','Your email has been activated','success');
        this.router.navigate(['/auth/login'])
      }
    } )
  }
  checkEmptyFields(){
    return (
      this.credentials.email.length === 0 ||
      this.credentials.password.length === 0
    );
  }

    loginSubmit() {
      this.isSubmitted = true ;
      if (this.checkEmptyFields()) {
        swal('Error', 'All fields must be filled in', 'error');
        return;
      }
      this.spinner.show();
       this.authService.login(this.credentials)
        .subscribe(
          (data: any) => {
            this.user = data.user;
            this.userService.changeUser(data.user);
            this.storageService.write(StorageService.USER_TOKEN_KEY, data.token);
            this.router.navigate(['/']).then(() => {
              location.reload();
            });
            this.spinner.hide();
          },
          (error) => {
            this.spinner.hide();
            if (error.status === 402) {
              swal({
                icon: 'error',
                title: 'Activate email',
                text: 'Email not activated yet!'
              });
              return ;
            }
            swal({
              icon: 'error',
              title: 'Invalid credentials...',
              text: 'You credentials are incorrect!'
            });
          }
        );

}
}
