import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SharedModule} from '../shared/shared.Module';
import {TranslateModule} from '@ngx-translate/core';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import { LoginComponent } from './login/login.component';
import { CreateUserComponent } from './create-user/create-user.component';
import { ManageUserRootingModule } from './manage-user.rooting.Module';
import { AuthService } from '../shared/services/auth.service';
import { InlineSVGModule } from 'ng-inline-svg';
import { ImageCropperModule } from 'ngx-image-cropper';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { UserProfileComponent } from './user-profile/user-profile.component';


@NgModule({
  declarations: [
    LoginComponent,
    CreateUserComponent,
    ForgetPasswordComponent,
    ResetPasswordComponent,
    UserProfileComponent
  ],
  imports: [
    CommonModule,
    InlineSVGModule,
    ImageCropperModule,
    SharedModule,
    TranslateModule,
    ManageUserRootingModule,
    InfiniteScrollModule,


  ],
  providers: [

  ]
})
export class ManageUserModule { }
