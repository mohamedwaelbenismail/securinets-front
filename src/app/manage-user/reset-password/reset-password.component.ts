import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UserService } from 'src/app/shared/services/user.service';
import { User } from 'src/app/shared/models/User';
import { sample } from 'rxjs/operators';
import { data } from 'jquery';

declare var swal ; 
@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
  pwd = '';
  isSubmitted = false;
  passwordError = false ; 
  verification_code: string;
  userId: number;
  busy: Subscription;
  constructor(private route: ActivatedRoute, private userService: UserService, private router: Router) {
    this.userId = parseInt(this.route.snapshot.queryParams.qrlfd, 10);
    this.userId = (this.userId - 5849) / 1589963;
    this.verification_code = this.route.snapshot.queryParams.verification_code;
   }

  ngOnInit(): void {
    this.userService.getUserByIdAndVerificationCode(this.userId, this.verification_code).subscribe((data) => {
    }, (error) => {
      swal('error', 'Not allowed' , 'error');
      this.router.navigate(['/auth/login']);
      return ;
    });
  }
  checkPassword() {
    let numdigits = 0;
    let numUperCase = 0;
    let numLowerCase = 0;
    let i = 0 ;
    if (this.pwd.length < 8) {
      return false;
    }
    while ( (numdigits < 1  || numUperCase < 1 || numLowerCase < 1)  && (i <= this.pwd.length - 1)) {
          const char = this.pwd.charAt(i) ;
          if (!isNaN( parseInt(char, 10) )){
            numdigits++ ;

          } else if (char === char.toUpperCase()) {
            numUperCase++ ;
          } else if (char === char.toLowerCase()) {
             numLowerCase++  ;
          }
          i++;
      }
    if (numdigits < 1 || numUperCase < 1 || numLowerCase < 1) {
        return false;
      }
    return true ;
  }
  resetPassword(){
    this.isSubmitted = true ;
    if (!this.checkPassword()) {
      swal('error', 'check your password', 'error');
      this.passwordError = true ;
      return ;
    }
    this.busy = this.userService.resetPassword(this.userId, this.pwd, this.verification_code).subscribe(() => {
      swal('success', 'success', 'success');
      this.router.navigate(['auth/login']);
    });
  }
}
