import { Component, OnInit } from '@angular/core';
import { UserService } from '../shared/services/user.service';

declare var swal : any;
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  name:string;
  email:string;
  message:string;
  constructor(private userService: UserService) { }

  ngOnInit(): void { 
  }
  navigateToLink(link) {
    window.open(link);
  }
  contact() {
    if (!this.name || !this.message || !this.email) {
      swal('warning','please fill all the form','warning');
      return ;
    }
    this.userService.contact(this.name,this.email,this.message).subscribe((data)=>{
      swal('success','message sent successfully','success');
    },(error) => {
      swal('error','error','error');
    })  
  }
  
}
