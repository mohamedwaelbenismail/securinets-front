import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-erro-page',
  templateUrl: './erro-page.component.html',
  styleUrls: ['./erro-page.component.scss']
})

export class ErroPageComponent implements OnInit {
  @Input() content:string;
  @Input() goTo: string;
  @Input() path: string ='/' ;
  @Input() action:string ;
  constructor() { }

  ngOnInit(): void {
  }

}
