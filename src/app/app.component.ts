import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {TranslateService} from '@ngx-translate/core';
import { UserService } from './shared/services/user.service';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'Securinets';
  articles: any[];
  constructor(translate: TranslateService, private router:Router,
    private http: HttpClient, private userService: UserService) {
    translate.setDefaultLang('fr');
    const lang = localStorage.getItem('lang');
    translate.use(lang ? lang : 'fr');
    this.userService.changeLanguage('fr');
  }
  ngOnInit() {
    this.router.events.subscribe((evt) => {
      if ((!(evt instanceof NavigationEnd))   ) {
          return;
      }
     if (evt.url.includes('event')) {
       return ;
     }
      window.scrollTo(0, 0);
  });
  }
}
