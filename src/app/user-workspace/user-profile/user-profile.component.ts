import {Component, OnInit, ViewChild} from '@angular/core';
import {ImageCroppedEvent} from 'ngx-image-cropper';
import {StorageService} from '../../shared/services/storage.service';
import {ActivatedRoute} from '@angular/router';
import {UserService} from '../../shared/services/user.service';
import {Subscription} from 'rxjs';
import {User} from '../../shared/models/User';
import {environment} from '../../../environments/environment';
import {NgxSpinnerService} from 'ngx-spinner';
import {NgForm} from '@angular/forms';
import {formatDate} from '@angular/common';

declare var swal: any;
declare var jQuery: any;

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {
  user = new User();
  busy: Subscription;
  backUrl = environment.baseUrl;
  oldPasswordWrong = false;
  passwordError = false;
  isSubmitted = false ;
  url: string | ArrayBuffer = '';
  editor: any;
  checked = false ;
  showModal: boolean;
  imageChangedEvent: any = null;
  croppedImage: any = null;
  image: File = null;
  imageToDisplay = null;
  users: User[];
  @ViewChild('userForm') public userForm: NgForm;
  formData = new FormData();
  offset = 0;
  perPage = 10;
  dtOptions: any = {};
  constructor(private userService: UserService,
              private activatedRoute: ActivatedRoute,
              private storageService: StorageService,
              private spinner: NgxSpinnerService
  ) {
    this.spinner.show();
  }

  ngOnInit() {
    this.getUser();
    this.dataTableForUserRaking();
    // this.getAllUserByPrivilege();
  }

  show() {
    if (this.imageChangedEvent) {
      this.showModal = true; // Show-Hide Modal Check
    }
  }

  hide() {
    this.showModal = false;
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
  }

  imageLoaded() {
    // show cropper
  }

  cropperReady() {
    // cropper ready
  }

  loadImageFailed() {
    // show message
  }
  saveCropped() {
    this.showModal = false;
    this.imageToDisplay = this.croppedImage;
    this.formData.append('photo', this.base64ToFile(this.croppedImage , 'user_profile'));
  }
  uploadUserPhoto(event) {
    this.image = event.target.files[0];
    if ( this.image.type.match(/image\/*/) == null) {
      swal('Warning', 'Only images are supported', 'warning');
      return;
    }
    const reader = new FileReader();
    reader.readAsDataURL(this.image);
    reader.onload = (_event) => {
      this.imageToDisplay = reader.result;
    };
    this.imageChangedEvent = event;
    this.formData.append('photo', this.image);

  }

  update() {
    if (!this.checked) {
      this.user.old_password = '';
      if (this.checkEmptyFields()) {
        swal('Warning', 'Please fill empty fields', 'warning');
        return ;
      }
      jQuery('#myModal').modal('show');
      return ;
    }
    if (this.checkEmptyFields()) {
      swal('Warning', 'Please fill empty fields', 'warning');
      return ;
    }
    else if (this.user.password.length > 0 &&  !this.checkPassword(this.user.password)) {
      this.isSubmitted = true;
      this.passwordError = true ;
      swal('Warning', 'Problem with password pattern', 'warning');
      return ;
    }
    else if ((this.user.password.length > 0 || this.user.confirmPassword.length > 0) &&
      this.user.password !== this.user.confirmPassword) {
      swal('Warning', 'Passwords must match', 'warning');
      return ;
    }
    if (this.userForm.valid) {
      for (const key of Object.keys(this.user)) {
        this.formData.append(key, this.user[key]);
      }
    }
    else {
      swal('Warning', 'Something went wrong !', 'warning');
      return ;
    }
    this.updateUser();
  }

  updateUser(){
    this.busy = this.userService.updateUserInfo(this.formData, this.checked).subscribe(
      data => {
        this.userService.changeUser(data);
        swal('Success!', 'Infos Updated', 'success');
        jQuery('#myModal').modal('hide');
        this.oldPasswordWrong = false;
        this.getUser();
      },
      error => {
        if (error.status === 500 && error.error.message === 'old password is wrong') {
          this.oldPasswordWrong = true ;
        }
        swal('Error!', 'The request failed!', 'error');
      }
    );
  }
  Submit() {
    if (!this.user.old_password ) {
      swal('Error!', 'Please enter your current password', 'error');
      return ;
    }
    for (const key of Object.keys(this.user)) {
      this.formData.append(key, this.user[key]);
    }
    this.updateUser();
  }
  getUser() {
    this.busy = this.userService.getUserFromToken().subscribe(data => {
      this.user = data;
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
    });
  }
  getAllUserByPrivilege() {
    this.userService.getAllUserByPrivilege(3, 0, 0).subscribe(data => {
      this.users = data;
    },
    error => {
      swal('Error', 'Ranking List not available', 'error');
    });
  }
  getColumns() {
    const that = this;
    const format = 'dd MMMM, yyyy';
    const locale = 'en-US';


    return   [
      {
        data: 'Rank',
        render: function(data, type, row) {
          return that.offset + that.users.findIndex(x => x.user_id === row.user_id) + 1 ;
        }
      },
      {
        data: 'First name',
        render: function(data, type, row) {
          return row.first_name ;
        }
      },
      {
        data: 'Last name',
        render: function(data, type, row) {
          return row.last_name ;
        }
      },
      {
        data: 'Points',
        render: function(data, type, row) {
          return row.points ;
        }
      },
      {
        data: 'Created at',
        render: function(data, type, row) {
          if (row.created_at) {
          return formatDate(row.created_at, format, locale); }
          return '';
        }
      },
    ];

  }
  dataTableForUserRaking() {
    const that = this;
    this.dtOptions = {
      dom: 'Bfrtip',
      buttons: [
        'colvis'
      ],
      serverSide: true,
      processing: true,
      pagingType: 'full_numbers',
      pageLength: this.perPage,
      responsive: true,
      searching: false,
      ordering:  false,
      columns: this.getColumns(),
      ajax: (dataTablesParameters: any, callback) => {
        const page = dataTablesParameters.start;
        this.offset = page;
        this.busy = this.userService.getAllUserByPrivilege(3 , page, this.perPage)
          .subscribe(
            (resp: any) => {
              this.users = resp.data;
              callback({
                data:  resp.data,
                recordsTotal: resp.to,
                recordsFiltered: resp.total,
              });
            },
            (error) => {
            }
          );
      }
    };
  }

  checkEmptyFields() {
    if (!this.checked) {
    return (
      !this.user.first_name ||
      !this.user.last_name ||
      !this.user.email
    );
    } else {
      return (
        !this.user.first_name ||
        !this.user.last_name ||
        !this.user.email ||
        !this.user.password ||
        !this.user.confirmPassword ||
        !this.user.old_password
      );
    }
  }

  checkPassword(password: string) {
    let numdigits = 0;
    let numUperCase = 0;
    let numLowerCase = 0;
    let i = 0;
    if (password.length < 8) {
      return false;
    }
    while ((numdigits < 1 || numUperCase < 1 || numLowerCase < 1) && (i <= password.length - 1)) {
      const char = password.charAt(i);
      if (!isNaN(parseInt(char, 10))) {
        numdigits++;

      } else if (char === char.toUpperCase()) {
        numUperCase++;
      } else if (char === char.toLowerCase()) {
        numLowerCase++;
      }
      i++;
    }
    if (numdigits < 1 || numUperCase < 1 || numLowerCase < 1) {
      return false;
    }
    return true;
  }

  base64ToFile(data, filename) {

    const arr = data.split(',');
    const mime = arr[0].match(/:(.*?);/)[1];
    const bstr = atob(arr[1]);
    let n = bstr.length;
    const u8arr = new Uint8Array(n);

    while (n--){
      u8arr[n] = bstr.charCodeAt(n);
    }

    return new File([u8arr], filename, { type: mime });
  }
}
