import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserWorkspaceRoutingModule } from './user-workspace-routing.module';
import {UserProfileComponent} from './user-profile/user-profile.component';
import {InlineSVGModule} from 'ng-inline-svg';
import {ImageCropperModule} from 'ngx-image-cropper';
import {SharedModule} from '../shared/shared.Module';
import {TranslateModule} from '@ngx-translate/core';
import {DataTablesModule} from 'angular-datatables';


@NgModule({
  declarations: [
    UserProfileComponent
  ],
  imports: [
    CommonModule,
    UserWorkspaceRoutingModule,
    InlineSVGModule,
    ImageCropperModule,
    SharedModule,
    TranslateModule,
    DataTablesModule
  ]
})
export class UserWorkspaceModule { }
