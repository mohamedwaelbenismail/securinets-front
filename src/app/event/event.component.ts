import {AfterViewInit, Component, OnInit} from '@angular/core';
import AOS from 'aos';
import {ActivatedRoute, Router, NavigationEnd} from '@angular/router';
import {NgxSpinner} from 'ngx-spinner/lib/ngx-spinner.enum';
import {NgxSpinnerService} from 'ngx-spinner';
import {Location} from '@angular/common';
import { StorageService } from '../shared/services/storage.service';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss']
})
export class EventComponent implements OnInit, AfterViewInit {
  arr = [1, 2, 3, 4];
  count = 1;
  year = 2019;
  index = 1;
  light = 0;
  lang : string ;
  constructor(private route: ActivatedRoute, private location: Location,
              private storageService : StorageService,
              private spinner: NgxSpinnerService, private router: Router) {
  }

  ngOnInit(): void {
    this.lang = localStorage.getItem('lang');
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0)
    });
    AOS.init({once: true}, );
    this.spinner.show();
    setTimeout(() => {
      this.spinner.hide();
    }, 1000);
    if (this.router.url.includes('NCSC')) {
      this.index = 2;
    }
    if (this.router.url.includes('SECURILIGHT')) {
      this.index = 3;
    }

  }

  ngAfterViewInit() {

  }

  onNext() {
    if (this.year != 2019) {
      this.year++;

    }
  }

  onNextL() {
    if (this.light != 2017) {
      this.light++;


    }
  }

  onReturn() {
    if (this.year != 2014) {
      this.year--;

    }
  }

  onReturnL() {
    if (this.light != 2014) {
      this.light--;
    }
  }


  indexSecuriday(index, light) {
    this.index = index;
    this.light = light;
    if (this.light !== 2017) {
      this.year = 2019;
    }
    setTimeout(() => {
      this.route.fragment.subscribe(f => {
        const element = document.querySelector('#' + f);
        if (element) {
          element.scrollIntoView();
        }
      });
    }, 20);

  }


}

