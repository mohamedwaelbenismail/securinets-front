import {Component, Input, OnInit, ViewChild, OnDestroy} from '@angular/core';
import {WorkshopRequest} from '../../shared/models/Workshop';
import {WorkshopService} from '../../shared/services/workshop.service';
import {Subscription} from 'rxjs';
import {NgForm} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';
import { JsonPipe } from '@angular/common';
import { UserService } from 'src/app/shared/services/user.service';
import { User } from 'src/app/shared/models/User';
import { Item_Evaluation } from 'src/app/shared/models/Item_Evaluation';

declare var swal;
declare var jQuery: any;
@Component({
  selector: 'app-add-workshop',
  templateUrl: './add-workshop.component.html',
  styleUrls: ['./add-workshop.component.scss']
})
export class AddWorkshopComponent implements OnInit {
  workshop: WorkshopRequest;
  busy: Subscription;
  addGrids = false ;
  grids : Item_Evaluation[] = [new Item_Evaluation()] ;
  @ViewChild('workshopForm') public workshopForm: NgForm;
  workshop_id: number;
  edit = false;
  loggedUser: User ;
  formData = new FormData();
  constructor(private route: ActivatedRoute,
              private workshopService: WorkshopService,
              private userSerivce: UserService,
              private spinner: NgxSpinnerService,
              private router: Router,
  ) {
    this.workshop_id = parseInt(this.route.snapshot.paramMap.get('workshop_id'), 0);
  }

  ngOnInit(): void {
    this.workshop = new WorkshopRequest();
    if (this.workshop_id) {
      this.spinner.show();
      this.userSerivce.loggedUser.subscribe(user => {
        this.loggedUser = user;
        this.getWorkshopById();
      });
    }
    (($) => {
      $('.custom-file-input').on('change', function() {
        const fileName = $(this).val().split('\\').pop();
        $(this).siblings('.custom-file-label').addClass('selected').html(fileName);
      });

    })(jQuery);
  }

  handleFileInput(files: FileList) {
    this.formData.append('photo', files.item(0)) ;
  }

  addWorkshop() {
    let that = this ;
    let error = false;
    if (!this.workshopForm.invalid) {
    if (this.workshopForm.valid) {
      for ( const key of Object.keys(this.workshop)) {
        this.formData.append(key, this.workshop[key]);
    }
    this.grids.forEach((grid) => {
      if (!grid.label) {
        swal('warning','please fill all the  labels', 'warning');
        error = true ;
        return ;
      }
    })
    if (error) return ;
    this.formData.append('grids', 
    this.addGrids ?
    JSON.stringify(this.grids) : JSON.stringify([]));
    
      if (this.edit) {
        this.busy = this.workshopService.editWorkshop(this.workshop_id, this.formData).subscribe(data => {
          this.workshopForm.reset();
          swal('success','success','success').then(function() {
            that.router.navigate(['/workshop/list']);
          })
          
        }, error => {
          swal('Error', 'An error has occurred', 'error');
        });
      } else {      
        this.busy = this.workshopService.addWorkshop(this.formData).subscribe(data => {
          this.workshopForm.reset();
          swal('success','success','success').then(function() {
            that.router.navigate(['/workshop/list']);
          })

        }, error => {
          swal('Error', 'An error has occurred', 'error');
        });
      }
    } else {
      swal('Warning', 'Form Invalid', 'warning');
    }
  }
  }

  getWorkshopById() {
    const from = 'edit';
    this.workshopService.getWorkshopById(this.workshop_id, from).subscribe(data => {
      this.edit = true;
      this.workshop = data;
      if (this.workshop.item_evaluation.length > 0 ) {
        this.addGrids = true ;
        this.grids = this.workshop.item_evaluation ;
      }
      if (this.workshop.moderator_id !== this.loggedUser.user_id) {
        this.router.navigate(['/workshop/list']);
        return;
      }
      this.workshop.start_date = this.workshop.start_date.split(' ').join('T');
      this.spinner.hide();

    }, error => {
      this.router.navigate(['/workshop/list']);
      this.spinner.hide();
    });

}
onChangeCheckBox(event) {
  this.addGrids = !this.addGrids;
}
actionOnLabel(action_type:string , i: number, grid : Item_Evaluation) {

  if (action_type == '1' && !grid.label) {
    swal('warning','the label field is required','warning');
    return;
  }
  if (action_type == '1' && grid.label) {
   this.grids.push(new Item_Evaluation());
 }
 if (action_type == '-1' && i == this.grids.length - 1 && this.grids.length > 1){
   this.grids.pop();
 }
 if (action_type == '-1' && i == this.grids.length - 1 && this.grids.length == 1){
  this.addGrids = false;
  this.grids = [new Item_Evaluation()];
}
}
}
