import {AfterViewInit, Component, OnInit, OnDestroy} from '@angular/core';
import {WorkshopService} from '../shared/services/workshop.service';
import {Workshop} from '../shared/models/Workshop';
import {Subscription} from 'rxjs';
import {NgxSpinnerService} from 'ngx-spinner';
import {Utils} from '../shared/utils';
import {UserService} from '../shared/services/user.service';
import {environment} from 'src/environments/environment';
import {User} from '../shared/models/User';
import {Workshop_User} from '../shared/models/Workshop_User';
import {Router, NavigationEnd} from '@angular/router';
import {StorageService} from '../shared/services/storage.service';
import {TranslateService} from '@ngx-translate/core';

declare var jQuery: any;
declare var swal : any;
@Component({
  selector: 'app-workshop',
  templateUrl: './workshop.component.html',
  styleUrls: ['./workshop.component.scss']
})
export class WorkshopComponent implements OnInit, OnDestroy {
  workshops: Workshop[];
  offset = 0;
  perPage = 4;
  search = '';
  busy: Subscription;
  notEmptyPost = true;
  notscrolly = true;
  spinnerShow = false;
  backUrl = environment.baseUrl;
  user: User = new User();
  user_privilege_id: number;
  joined = [];
  choice = 'null';
  show = [];

  private lastPoppedUrl: string;
  private yScrollStack: number[] = [];
  workshopsFilter = [
    {
      'key': 'WORKSHOP.AllWorkshops',
      'value': 'null'
    },
    {
      'key': 'WORKSHOP.MyWorkshops',
      'value': null
    }
  ];

  constructor(private workshopService: WorkshopService, private router: Router,
              private storageService: StorageService, private translateService: TranslateService,
              protected userService: UserService, private spinner: NgxSpinnerService) {
  }

  ngOnInit(): void {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0);
    });

    this.spinner.show();
    this.busy = this.userService.loggedUser.subscribe((data) => {
      this.user = data;
      if (this.user) {
        this.workshopsFilter[1].value = this.user.user_id.toString();
      }

    });
    this.getAllWorkshopsWithPagination();
  }

  ngOnDestroy() {
    this.busy.unsubscribe();
  }

  selectChoice(value) {
    this.choice = value;
  }

  getAllWorkshopsWithPagination() {
    this.busy = this.workshopService.getAllWorkshops(this.offset, this.perPage, this.search, this.choice).subscribe(data => {
        this.workshops = data;
        this.workshops.forEach((work) => {
          this.show.push(this.compareDate(work.start_date));
        });
        this.spinner.hide();
      },
      error => {
        this.spinner.hide();
      });
  }

  compareDate(date) {
    if (new Date(date) < new Date()) {
      return true;
    }
    if (new Date(date) > new Date()) {
      return false;
    }
  }

  manageDescription(i: number) {
    const cl = '.desc' + i;
    Utils.onHoverEffect('.post-module', cl);
  }

  loadNextWorkshops() {
    this.spinnerShow = true;
    this.offset += this.perPage;
    this.workshopService.getAllWorkshops(this.offset, this.perPage, this.search, this.choice)
      .subscribe((data: Workshop[]) => {
        if (data.length === 0) {
          this.notEmptyPost = false;
        }
        this.paginating(data);
        this.notscrolly = true;
        this.spinnerShow = false;

      });
  }

  onScroll() {
    if (this.notscrolly && this.notEmptyPost) {
      this.notscrolly = false;
      this.loadNextWorkshops();
    }
  }

  paginating(data: Workshop[]) {
    for (const workshop of data) {
      this.show.push(this.compareDate(workshop.start_date));
      this.workshops.push(workshop);
    }
  }

  filter() {
    this.offset = 0;
    this.notscrolly = true;
    this.notEmptyPost = true;
    this.spinner.show();
    this.getAllWorkshopsWithPagination();
  }

  joinWorkshop(workshopId: number) {
    if (!this.user) {
      jQuery('#myModal').modal('show');
      return;
    }
    const workshop_user = new Workshop_User();
    workshop_user.workshop_id = workshopId;
    this.workshopService.joinWorkshop(workshop_user)
      .subscribe(data => {
        swal('success','success','success').then(function() {
          location.reload();
        })
        
      });
  }

  redirectTo(action: number) {

    jQuery('#myModal').modal('hide');
    if (action == 1) {
      this.router.navigate(['/auth/login']);
    } else {
      this.router.navigate(['/auth/create-account']);
    }
  }

  quitWorkshop(workshopId: number) {
    this.workshopService.quitWorkshop(workshopId)
      .subscribe(data => {
        swal('success','success','success').then(function() {
          location.reload();
        })
      });
  }
}
