import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {WorkshopComponent} from './workshop.component';
import {WorkshopDetailComponent} from './workshop-detail/workshop-detail.component';
import {AddWorkshopComponent} from './add-workshop/add-workshop.component';
import { AdminGuard } from '../shared/guards/adminGuard.guard';


const routes: Routes = [
  { path: 'list', component: WorkshopComponent },
  { path: 'detail/:workshop_id', component: WorkshopDetailComponent },
  { path: 'add', component: AddWorkshopComponent, canActivate: [AdminGuard]},
  { path: 'edit/:workshop_id', component: AddWorkshopComponent, canActivate: [AdminGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WorkshopRoutingModule { }
