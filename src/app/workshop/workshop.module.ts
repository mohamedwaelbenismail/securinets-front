import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WorkshopRoutingModule } from './workshop-routing.module';
import {WorkshopComponent} from './workshop.component';
import {WorkshopDetailComponent} from './workshop-detail/workshop-detail.component';
import {AddWorkshopComponent} from './add-workshop/add-workshop.component';
import {WorkshopService} from '../shared/services/workshop.service';
import {SharedModule} from '../shared/shared.Module';
import {TranslateModule} from '@ngx-translate/core';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import {FormsModule} from '@angular/forms';
import {NgBusyModule} from 'ng-busy';
import {MatFormFieldModule} from '@angular/material/form-field';
import { ErroPageComponent } from '../erro-page/erro-page.component';
@NgModule({
  declarations: [
    WorkshopComponent,
    WorkshopDetailComponent,
    AddWorkshopComponent,
    ErroPageComponent
  ],
  imports: [
    CommonModule,
    WorkshopRoutingModule,
    SharedModule,
    TranslateModule,
    InfiniteScrollModule,
    FormsModule,
    MatFormFieldModule
  ],
  providers: [
    WorkshopService,
  ]
})
export class WorkshopModule { }
