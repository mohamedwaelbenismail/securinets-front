import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {User} from 'src/app/shared/models/User';
import {WorkshopService} from 'src/app/shared/services/workshop.service';
import {UserService} from 'src/app/shared/services/user.service';
import {Workshop, WorkshopRequest} from 'src/app/shared/models/Workshop';
import {Workshop_User} from 'src/app/shared/models/Workshop_User';
import {Subscription} from 'rxjs';
import {StorageService} from 'src/app/shared/services/storage.service';
import {environment} from 'src/environments/environment';

declare var swal;
declare var jQuery;

@Component({
  selector: 'app-workshop-detail',
  templateUrl: './workshop-detail.component.html',
  styleUrls: ['./workshop-detail.component.scss']
})

export class WorkshopDetailComponent implements OnInit {
  workshop_id: number;
  logged_user: User;
  backUrl = environment.baseUrl;
  busy: Subscription;
  joined = false;
  is_moderator: boolean;
  workshop: Workshop = new Workshop();
  users: User[];
  all_workshop_users = new Array<Workshop_User>();

  constructor(private route: ActivatedRoute, private workshopService: WorkshopService,
              private storageService: StorageService,
              private userService: UserService, private router: Router) {
    this.workshop_id = parseInt(this.route.snapshot.paramMap.get('workshop_id'), 0);
  }

  ngOnInit(): void {
    if (this.workshop_id) {
      this.userService.loggedUser.subscribe((user => {
        this.logged_user = user;
      }));
      this.getWorkshopById();
    }
  }

  joinWorkshop() {
    if (!this.logged_user) {
      jQuery('#myModal').modal('show');
      return;
    }
    const workshop_user = new Workshop_User();
    workshop_user.workshop_id = this.workshop.workshop_id;
    this.workshopService.joinWorkshop(workshop_user)
      .subscribe(data => {
      });
  }

  redirectTo(action: number) {
    jQuery('#myModal').modal('hide');
    if (action == 1) {
      this.router.navigate(['/auth/login']);
    } else {
      this.router.navigate(['/auth/create-account']);
    }
  }

  getWorkshopById() {
    this.busy = this.workshopService.getWorkshopById(this.workshop_id).subscribe(data => {
      this.workshop = data;
      this.is_moderator = data.isModerator;
      if (this.is_moderator && (this.logged_user.user_id !== this.workshop.moderator_id)) {
        this.router.navigate(['/workshop/list']);
      }
    }, error => {
      this.router.navigate(['/workshop/list']);
    });
  }

  savePresence(workshopId: number) {
    let error = false; 
    if (this.workshop.item_evaluation.length > 0) {
      this.workshop.workshop_users.forEach((wu) => {
        wu.workshop_user_evaluation.forEach((eva) => {
          if (eva.note > 20 || eva.note < 0) {
            swal('warning','The mark must be between 0 and 20','warning');
            error = true ;
            return ;
          }
          if (wu.is_present == false ) {
            eva.note = 0;
          }
        })
      })
      if (error) return ;
    }
    this.busy = this.workshopService.savePresence(this.workshop.workshop_users, this.workshop_id).subscribe(
      (data) => {
        this.router.navigate(['/workshop/list']);
      }, (error) => {
        swal('Error', 'an Error has occurred', 'error');


      }
    );
  }

}
