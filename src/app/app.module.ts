import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

import {NgMediumModule} from 'ng-medium';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.Module';
import {HTTP_INTERCEPTORS,  HttpClient, HttpClientModule } from '@angular/common/http';
import { HttpConfigInterceptor } from './shared/interceptors/httpconfig.interceptor';
import { FullLayoutComponent } from './full-layout/full-layout.component';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { ManageUserModule } from './manage-user/manage-user.Module';
import { StorageService } from './shared/services/storage.service';
import { AuthService } from './shared/services/auth.service';
import { NgBusyModule } from 'ng-busy';
import { ForgetPasswordComponent } from './manage-user/forget-password/forget-password.component';
import { ResetPasswordComponent } from './manage-user/reset-password/reset-password.component';
import { EventComponent } from './event/event.component';

import { UserProfileComponent } from './manage-user/user-profile/user-profile.component';
import { FooterComponent } from './footer/footer.component';

@NgModule({
  declarations: [
    AppComponent,
    FullLayoutComponent,
    HomeComponent,
    AboutComponent,

    EventComponent,

    FooterComponent,


  ],
    imports: [
        MDBBootstrapModule.forRoot(),
        BrowserModule,
        SharedModule,
        // ManageUserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        NgMediumModule,
        MDBBootstrapModule.forRoot(),
        BrowserAnimationsModule,


        HttpClientModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: (createTranslateLoader),
                deps: [HttpClient],

            }
        }),
    ],
  providers: [
    StorageService,
    AuthService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpConfigInterceptor,
      multi: true
    }
      ],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
