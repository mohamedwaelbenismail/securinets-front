import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateUserComponent } from './manage-user/create-user/create-user.component';
import { FullLayoutComponent} from './full-layout/full-layout.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { AuthGuard } from './shared/guards/check-user-right-guard.guard';
import {EventComponent} from './event/event.component'

import { UserProfileComponent } from './manage-user/user-profile/user-profile.component';
import { ErroPageComponent } from './erro-page/erro-page.component';

const routes: Routes = [
  {
    path: '',
    component: FullLayoutComponent,
    children: [
      {
        path: '',
        component: HomeComponent
      },
      {
        path: 'user-profile/:id',
        component: UserProfileComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'about',
        component: AboutComponent
      },
      {
        path: 'event',
        component: EventComponent
      },
      {  path: 'auth',
      loadChildren: () => import('./manage-user/manage-user.Module').then(m => m.ManageUserModule),
      canActivate: [AuthGuard]
      },
      {  path: 'user-workspace',
        loadChildren: () => import('./user-workspace/user-workspace.module').then(m => m.UserWorkspaceModule),
        canActivate: [AuthGuard]
      },
      {  path: 'workshop',
      loadChildren: () => import('./workshop/workshop.module').then(m => m.WorkshopModule)
      }
  ]
  },
  {path: '**', component: ErroPageComponent}
];
@NgModule({
  imports: [RouterModule.forRoot(routes,{
    anchorScrolling: 'enabled'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
