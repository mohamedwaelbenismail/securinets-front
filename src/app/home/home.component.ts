import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {UserService} from '../shared/services/user.service';
import AOS from 'aos';
import {Utils} from '../shared/utils';
import {NgxSpinnerService} from 'ngx-spinner';
import {data} from 'jquery';
import {NavigationEnd, Router} from '@angular/router';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  li_4: string;
  name: string;
  email: string;
  message: string;

  lang: string;
  index = 3;
  articles: any[] = [];
  articlesLeft: any[] = [];
  images: any[];
  responsiveOptions: any[] = [
    {
      breakpoint: '1024px',
      numVisible: 5
    },
    {
      breakpoint: '960px',
      numVisible: 4
    },
    {
      breakpoint: '768px',
      numVisible: 3
    },
    {
      breakpoint: '560px',
      numVisible: 1
    }
  ];


  constructor(private translate: TranslateService,
              private spinner: NgxSpinnerService,
              private userService: UserService,
              private http: HttpClient,
              private  router: Router) {
  }

  ngOnInit(): void {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0)
    });
    this.images = [
      {url: 'https://cdn.pixabay.com/photo/2017/09/07/08/54/money-2724241__340.jpg', title: 'Image 1'},
      {url: 'https://cdn.pixabay.com/photo/2015/07/27/20/16/book-863418__340.jpg', title: 'Image 2'},
      {url: 'https://cdn.pixabay.com/photo/2017/08/30/07/56/money-2696229__340.jpg', title: 'Image 3'},
      {url: 'https://cdn.pixabay.com/photo/2016/10/09/19/19/coins-1726618__340.jpg', title: 'Image 4'},


    ];
    // location.reload();
    AOS.init({ once: true}, );
    Utils.initVenobox();
    this.userService.lang.subscribe((lang) => {
      this.lang = lang;
    });
    this.translate.get('li_4').subscribe((data) => {
      this.li_4 = data;

    });
    this.getmeduim();
  }
  navigateToPage(){
    console.log('hey');
  }
  getmeduim() {
    this.userService.meduim().subscribe((data: any) => {
        this.articles = data.items ;
    })
  }

  contact() {
    this.userService.contact(this.name, this.email, this.message).subscribe((data: any) => {
    });


  }


}


