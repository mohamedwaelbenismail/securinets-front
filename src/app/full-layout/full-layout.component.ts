import {Component, OnInit, HostListener} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {Subscription} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {UserService} from '../shared/services/user.service';
import {User} from '../shared/models/User';
import {Router} from '@angular/router';
import {StorageService} from '../shared/services/storage.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {data} from 'jquery';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

declare var jQuery: any;
declare var $;

@Component({
  selector: 'app-full-layout',
  templateUrl: './full-layout.component.html',
  styleUrls: ['./full-layout.component.scss']
})
export class FullLayoutComponent implements OnInit {
  selectedLang = 'fr';
  user: User;
  loadedUser = null;
  isToken: boolean;
  // l'utlité mtaa il variable hethi, kif tabda feha valeur naffichiw soit le profile soit button
  // si non inajem miloul yaffichi se connecter ou baad kif illoadi il user ibadlha, ili houa bizarre
  // nhebou yafichi fard marra ;
  flags = {
    fr: '../../assets/images/flags/fr.svg',
    en: '../../assets/images/flags/gb.svg',
  };
  name: string;
  email: string;
  message: string;
  className : string ;
  aricles : any;
  url = '';
  constructor(private translate: TranslateService,
              public userService: UserService,
              private router: Router,
              private http: HttpClient,
              private storageService: StorageService,
              private spinner: NgxSpinnerService
  ) {

  }

  ngOnInit(): void {
    if (this.storageService.isExist(StorageService.USER_TOKEN_KEY)) {
      this.isToken = true;
    } else {
      this.isToken = false;
    }
    if (localStorage.getItem('lang')) {
      this.selectedLang = localStorage.getItem('lang');
      this.userService.changeLanguage(this.selectedLang);
    }
    this.userService.loggedUser.subscribe(user => {
      this.user = user;
    });
    // refresh case
    if (this.storageService.isExist(StorageService.USER_TOKEN_KEY) && !this.user) {
      this.userService.getUserFromToken().subscribe((user: User) => {
          this.userService.changeUser(user);
          this.userService.loggedUser.subscribe(data => {
            this.user = data;
            this.loadedUser = true;
          });
        }, error => {
          this.logout();
        }
      );
    } else if (!this.user) {
      this.loadedUser = false;
    } else {
      this.loadedUser = true;
    }
    this.url = this.router.url 
    if (this.url == '/' ) {
      this.url = "home";
    } else if (this.url == "/event") {
      this.url = "event";
    } else {
      this.url="workshop";
    }
    
  }
  ngAfterViewInit() {
    $( '#header .nav-menu' ).find( 'li.menu-active' ).removeClass( 'menu-active' );
    $( '#header .nav-menu' ).find( `li.${this.url}` ).addClass( 'menu-active' );
    $( '#header .nav-menu a' ).on( 'click', function () {
      $( '#header .nav-menu' ).find( 'li.menu-active' ).removeClass( 'menu-active' );
      $( this ).parent( 'li' ).addClass( 'menu-active' );
    });
  }
  changeClassFunction(){
      var x = document.getElementById("myTopnav");
  if (x.className === "topnav") {
    x.className += " responsive";
    this.className = x.className;
  } else {
    x.className = "topnav";
    this.className = '';
  }
  }
  navigateToContact(className: string) {
    const elementList = document.querySelectorAll('#' + className);
    const element = elementList[0] as HTMLElement;
    element.scrollIntoView({behavior: 'smooth'});
  }

  reload(route: string) {

    this.router.navigate([route]).then(() => {
      location.reload();
    });
  }

  logout() {
    this.spinner.show();
    setTimeout(() => {
      this.loadedUser = false;
      this.user = null;
      this.isToken = false;
      this.storageService.removeAll();
      this.userService.changeUser(null);
      this.router.navigate(['/']).then(() => {
        this.spinner.hide();
        location.reload();
      });
    }, 500);
  }


  // @HostListener('window:scroll', []) onWindowScroll() {
  //   const blogNav = '.nav-link';
  //   console.log(blogNav);
  //   (($) => {
  //     if ($(blogNav).offset().top > 50) {
  //       console.log('in');
  //       $(blogNav).addClass('black');
  //     } else {
  //       $(blogNav).removeClass('black');
  //     }
  //   })(jQuery);
  //   }


  changeLang(lang: string) {
    this.spinner.show();
    this.selectedLang = lang;
    this.translate.use(lang);
    this.userService.changeLanguage(lang);
    localStorage.setItem('lang', lang);
    setTimeout(() => this.spinner.hide(), 600);
  }

}
