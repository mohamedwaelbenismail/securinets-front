import {HttpClient} from '@angular/common/http';
import {catchError} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {Credentials} from '../models/credentials';
import {StorageService} from './storage.service';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';


@Injectable()
export class AuthService  {

  constructor(private http: HttpClient, private storageService: StorageService) {
  }

  login(credentials: Credentials) {
    const url = environment.baseUrl + '/auth/login';
    return this.http.post<any>(url, credentials);
  }

  isLoggedIn() {
    return this.storageService.read(StorageService.USER_TOKEN_KEY) != null;
  }

  requestReset(body): Observable<any> {
    return this.http.post(`${environment.baseUrl}/guest/forgotPassword`, body);
  }

}
