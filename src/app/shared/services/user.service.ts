import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { User } from '../models/User';
import { HttpClient } from '@angular/common/http';
import { StorageService } from './storage.service';
import { Subject } from 'rxjs';
import {first} from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private userSource = new BehaviorSubject<User>(null);
  private language = new BehaviorSubject<string>(null);
  loggedUser = this.userSource.asObservable();
  lang = this.language.asObservable();
  constructor(private http: HttpClient, private storageService: StorageService) {

  }

  changeUser(user: User) {
    this.userSource.next(user);
  }
  changeLanguage(lang:string) {
    this.language.next(lang);
  }
  addUser(user: User) {
    const url = environment.baseUrl + '/users/register';
    return this.http.post(url, user);
  }
  forgetPassword(email: string) {
    const url = environment.baseUrl + '/users/password/reset';
    const body = {email};
    return this.http.post(url, body);
  }
  resetPassword(user_id: number, password: string, verification_code: string) {
    const url = environment.baseUrl + '/users/password/reset/' + user_id + '/' + verification_code;
    const body = { password };
    return this.http.post(url, body);
  }

  getUserByIdAndVerificationCode(user_id: number, verification_code: string) {
    const url = environment.baseUrl + '/users/byId/' + user_id + '/verificationCode/' + verification_code;
    return this.http.get(url);
  }

  getUserFromToken() {
    const url = environment.baseUrl + '/users/getUserByToken';
    return this.http.get<any>(url);
  }
  updateUserInfo(user, checked: boolean) {
    const url = environment.baseUrl + '/users/update-info?checked=' + checked;
    return this.http.post<any>(url, user);
  }
  getAllUserByPrivilege(privilege: number, page: number, perPage: number) {
    const url = environment.baseUrl + '/users/all/' + privilege + `?offset=${page}&perPage=${perPage}`;
    return this.http.get<any>(url);
  }
  contact(name: string, email: string, message: string){
    const url=environment.baseUrl+'/users/contact-us';
    return this.http.post(url,{name:name,email:email,message:message})
  }
  meduim(){
   return this.http.get(
    'https://api.rss2json.com/v1/api.json?rss_url=https://medium.com/feed/@Securinets'

   )
};
}
