import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Workshop, WorkshopRequest} from '../models/Workshop';
import {Workshop_User} from '../models/Workshop_User';
import { Item_Evaluation } from '../models/Item_Evaluation';

@Injectable({
  providedIn: 'root'
})
export class WorkshopService {
  baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) {
  }


  getAllWorkshops(offset: number, perPage: number, search: string,moderateur_id:string) {
    const url = this.baseUrl + `/workshop/all?offset=${offset}&perPage=${perPage}&moderateur_id=${moderateur_id}&search=${search}`;
    return this.http.get<Workshop[]>(url);
  }

  editWorkshop(workshop_id: number, formData: FormData) {
    const url = this.baseUrl + `/workshop/edit/${workshop_id}`;
    return this.http.post(url, formData);
  }

  addWorkshop(formData: FormData) {
    const url = this.baseUrl + `/workshop/add`;
    return this.http.post(url, formData);
  }

  deleteWorkshop(workshop_id: number) {
    const url = this.baseUrl + `/workshop/delete/${workshop_id}`;
    return this.http.delete(url);
  }

  getLoggedInUserWorkshops() {
    const url = this.baseUrl + `/workshop/me`;
    return this.http.get<Workshop[]>(url);
  }

  getWorkshopById(workshop_id: number, from?: string) {
    const url = this.baseUrl + `/workshop/detail/${workshop_id}?from=${from}`;
    return this.http.get<any>(url);
  }

  joinWorkshop(workshop_user: Workshop_User) {
    const url = this.baseUrl + `/workshop/join`;
    return this.http.post<any>(url, workshop_user);
  }

  quitWorkshop(workshopId: number) {
    const url = this.baseUrl + `/workshop/quit/${workshopId}`;
    return this.http.delete<any>(url);
  }

  getAllUsersByWorkshopId(workshopId: number) {
    const url = this.baseUrl + `/workshop/get-users/${workshopId}`;
    return this.http.get<any>(url);
  }

  savePresence(workshop_users: Workshop_User[], workshop_id: number) {
    const url = this.baseUrl + `/workshop/save-presence/${workshop_id}`;
    return this.http.post<any>(url, workshop_users);
  }
}
