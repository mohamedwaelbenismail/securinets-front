import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { UserService } from '../services/user.service';
import { User } from '../models/User';
import { map, catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {
  user: User;
  constructor(private userService: UserService, private router: Router ){
    this.userService.loggedUser.subscribe((user) => {
          this.user = user;
      });
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
     return this.checkIfAdmin(next.url[0].path);
  }
 // La logique: kif trefreshi oula typi il url manuellement, vu que il guards yetlancew kbal components donc il user dima yarjaa null 
 // heka aaleh aamlit il else if, si non ken famech refresh ntasti aadi 
  checkIfAdmin(url): boolean | Observable<boolean>{
      if (this.user && this.user.privilege_id === 3  ) {
          this.router.navigate(['']);
          return false;
      } else if (!this.user) {
         return this.userService.getUserFromToken().pipe(map( (user: User) => {
        if (user.privilege_id === 3) {
          this.router.navigate(['']);
          return false;
        }
        return true ;
    }), catchError((error) => {
            this.router.navigate(['']);
            return of(false);
        }));
      } else {
          return true ;
      }
      }


}
