import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { UserService } from '../services/user.service';
import { User } from '../models/User';
import { map, catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  user: User;
  constructor(private userService: UserService, private router: Router ){
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
     return this.checkLoginUser(next.url[0].path);
  }
  // vu qui typi il url manuallement yaamel refresh ou vu que  il guards yetlancew kbal il components il user dima yarjaa null
  // heka aaaleh aamlit http request;
  checkLoginUser(url): Observable<boolean>{
    return this.userService.getUserFromToken().pipe(map(response => {
        if (url === 'auth') {
          this.router.navigate(['']);
          return false;
        }
        return true ;
    }), catchError((error) => {
            return of(true);
        }));
      }

}
