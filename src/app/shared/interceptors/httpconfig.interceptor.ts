import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse
} from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { StorageService } from '../services/storage.service';
import { UserService } from '../services/user.service';

@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {
  constructor(private router: Router, private storageService: StorageService, private userService: UserService) {

  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let token: string = localStorage.getItem('user-token');
    if (token && request.url !== "https://api.rss2json.com/v1/api.json?rss_url=https://medium.com/feed/@Securinets") {
      request = request.clone({headers: request.headers.set('Authorization', 'Bearer ' + token.slice(1, -1))});
    }
    return next.handle(request).pipe(
      catchError((error) => {
        if (error.status === 403 && error.error.token) {
          this.storageService.write(StorageService.USER_TOKEN_KEY, error.error.token);
          token = localStorage.getItem('user-token');
          request = request.clone({headers: request.headers.set('Authorization', 'Bearer ' + token.slice(1, -1))});
          return next.handle(request);
        }
        return throwError(error);
      })
    );
  }

}

