declare var jQuery;
declare var $:any;
export class Utils {



    static onHoverEffect(className: string, cl: string) {
        (($) => {
            $(className).find(cl).stop().animate({
              height: 'toggle',
              opacity: 'toggle'
            }, 300);
        })(jQuery);
      }
      public static initVenobox() {
        $(window).on('load', function() {
          $('.venobox').venobox({
            bgcolor: '',
            overlayColor: 'rgba(6, 12, 34, 0.85)',
            closeBackground: '',
            closeColor: '#fff',
            share: false
          });
        });
      }

}




