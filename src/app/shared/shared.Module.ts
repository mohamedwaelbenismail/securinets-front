import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SpinnerComponent} from './shared-component/spinner/spinner.component';
import {LoadingSpinnerComponent} from './shared-component/loading-spinner/loading-spinner.component';
import {NgxSpinnerModule} from 'ngx-spinner';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import { FormsModule } from '@angular/forms';
import { NgBusyModule } from 'ng-busy';
import { TranslateModule } from '@ngx-translate/core';
import { ErroPageComponent } from '../erro-page/erro-page.component';

@NgModule({
  declarations: [
    SpinnerComponent,
    LoadingSpinnerComponent
  ],
  exports: [
    SpinnerComponent,
    LoadingSpinnerComponent,
    FormsModule,
    NgBusyModule,
    
  ],
  imports: [
    CommonModule,
    NgxSpinnerModule,
    InfiniteScrollModule,
  ]
})
export class SharedModule { }
