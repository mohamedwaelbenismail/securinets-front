import { Item_Evaluation } from './Item_Evaluation';
import {User} from './User';
import { Workshop_User } from './Workshop_User';

export class Workshop {
  workshop_id: number;
  name: string;
  description: string;
  moderator: User;
  moderator_id: number;
  duration: number;
  start_date: Date;
  class_room: string;
  max_places: number;
  available_places: number;
  url_photo:string;
  workshop_users:Workshop_User[];
  item_evaluation: Item_Evaluation[];
}
export class WorkshopRequest {
  workshop_id: number;
  name: string;
  description: string;
  duration: number;
  moderator_id: number;
  start_date: string;
  class_room: string;
  max_places: number;
  item_evaluation: Item_Evaluation[];
}
