import { Workshop_User } from './Workshop_User';
import { Workshop, WorkshopRequest } from './Workshop';

export class User {
  user_id: number;
  membership_index : number;
  membership_id : number;
  first_name: string;
  last_name: string;
  password: string;
  confirmPassword: string;
  email: string;
  points: number = 0;
  privilege_id: number;
  password_reset = 0 ;
  created_at: Date;
  is_present:boolean=false;
  user_workshops:Workshop_User[];
  workshop: WorkshopRequest;
  old_password: string;
  url_photo: string;
}
