import {User} from './User';
import { Workshop_User_Evaluation } from './Workshop_User_Evaluation';

export class Workshop_User {
  workshop_user_id: number;
  user_id:number;
  workshop_id:number;
  is_present:boolean;
  workshop_user_evaluation: Workshop_User_Evaluation[];
}